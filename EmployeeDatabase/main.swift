//
//  main.swift
//  EmployeeDatabase
//
//  Created by robin on 2018-01-15.
//  Copyright © 2018 robin. All rights reserved.
//

import Foundation

// This code is from App Development with Swift by Apple Inc (2017)
class Student {
    let name: String
    let id: String
    init(sName: String, sId:String) {
        self.name = sName
        self.id = sId
    }
    
    func sayHello() {
        print("Hello, \(self.name)! Your college id is: \(self.id)")
    }
    
}

let person1 = Student(sName: "Prabjyot", sId:"20178399")
let person2 = Student(sName: "Ramandeep Kaur", sId:"20170234")
let person3 = Student(sName: "Aisha Patel", sId:"20174933")
let person4 = Student(sName: "Emad Nasarali", sId:"20169800")

person1.sayHello()
person2.sayHello()
person3.sayHello()
person4.sayHello()







